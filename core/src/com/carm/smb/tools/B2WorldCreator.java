package com.carm.smb.tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.carm.smb.SMB;

/**
 * Created by carlos on 13/01/2017.
 */

public class B2WorldCreator {

    public B2WorldCreator(World world, TiledMap map) {
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fedf = new FixtureDef();

        Body body;

        for (MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / SMB.PPM, (rect.getY() + rect.getHeight() / 2) / SMB.PPM);

            body = world.createBody(bdef);

            shape.setAsBox((rect.getWidth() / 2) / SMB.PPM, (rect.getHeight() / 2) / SMB.PPM);
            fedf.shape = shape;
            body.createFixture(fedf);
        }

        for (MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / SMB.PPM, (rect.getY() + rect.getHeight() / 2) / SMB.PPM);

            body = world.createBody(bdef);

            shape.setAsBox((rect.getWidth() / 2) / SMB.PPM, (rect.getHeight() / 2) / SMB.PPM);
            fedf.shape = shape;
            body.createFixture(fedf);
        }

        for (MapObject object : map.getLayers().get(8).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / SMB.PPM, (rect.getY() + rect.getHeight() / 2) / SMB.PPM);

            body = world.createBody(bdef);

            shape.setAsBox((rect.getWidth() / 2) / SMB.PPM, (rect.getHeight() / 2) / SMB.PPM);
            fedf.shape = shape;
            body.createFixture(fedf);
        }

        for (MapObject object : map.getLayers().get(9).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / SMB.PPM, (rect.getY() + rect.getHeight() / 2) / SMB.PPM);

            body = world.createBody(bdef);

            shape.setAsBox((rect.getWidth() / 2) / SMB.PPM, (rect.getHeight() / 2) / SMB.PPM);
            fedf.shape = shape;
            body.createFixture(fedf);
        }
    }

}
